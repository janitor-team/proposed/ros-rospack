ros-rospack (2.6.2-8) unstable; urgency=medium

  * Simplify d/rules
  * Move /usr/share/ to the end of ROS_PACKAGE_PATH.
    Thanks to Lucas Walter
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 16 May 2022 23:12:15 +0200

ros-rospack (2.6.2-7) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * Fix Multiarch hinter issues

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:44:32 +0200

ros-rospack (2.6.2-6) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Fri, 03 Sep 2021 18:21:26 +0200

ros-rospack (2.6.2-5) unstable; urgency=medium

  * Revert "reduce dependencies"
  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 12:02:21 +0100

ros-rospack (2.6.2-4) unstable; urgency=medium

  * reduce dependencies

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 16 Dec 2020 23:33:05 +0100

ros-rospack (2.6.2-3) unstable; urgency=medium

  * bump policy version (no changes)
  * Drop Python dependency (Closes: #976049)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 29 Nov 2020 11:49:46 +0100

ros-rospack (2.6.2-2) unstable; urgency=medium

  * include ros_environment in rospack unit test (Closes: #963410)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 25 Jun 2020 21:42:43 +0200

ros-rospack (2.6.2-1) unstable; urgency=medium

  * simplify d/watch
  * New upstream version 2.6.2
  * Remove Thomas from Uploaders, thanks for working on this
  * rebase patches
  * bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 13:58:12 +0200

ros-rospack (2.5.4-2) unstable; urgency=medium

  * Add catkin_test_results

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 09 Nov 2019 18:53:54 +0100

ros-rospack (2.5.4-1) unstable; urgency=medium

  * Guard override_dh_auto_test
  * New upstream version 2.5.4
  * Rebase patches
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Oct 2019 21:45:12 +0200

ros-rospack (2.5.3-2) unstable; urgency=medium

  * Move to Python 3 (Closes: #938399)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 27 Sep 2019 13:35:16 +0200

ros-rospack (2.5.3-1) unstable; urgency=medium

  * New upstream version 2.5.3
  * rebase patches
  * Bump policy version (no changes)
  * switch to debhelper-compat and debhelper 12
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 31 Jul 2019 20:21:26 +0200

ros-rospack (2.5.2-1) unstable; urgency=medium

  * Remove get-orig-source
  * New upstream version 2.5.2
  * Bump policy version (no changes)
  * rebase patches
  * simplify d/rules

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 10 Oct 2018 22:57:42 +0200

ros-rospack (2.5.1-3) unstable; urgency=medium

  * Annotate roslang build-dep to break cycle
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 23 Aug 2018 18:47:59 +0200

ros-rospack (2.5.1-2) unstable; urgency=medium

  * Add newline to warning

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 18 Jun 2018 22:19:57 +0200

ros-rospack (2.5.1-1) unstable; urgency=medium

  * New upstream version 2.5.1

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 16 Jun 2018 16:32:06 +0200

ros-rospack (2.5.0-2) unstable; urgency=medium

  [ Steven Robbins ]
  * Link tests with -pthread.  Closes: #895708.

  [ Jochen Sprickerhof ]
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 17 Apr 2018 23:42:53 +0200

ros-rospack (2.5.0-1) unstable; urgency=medium

  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * New upstream version 2.5.0
  * Rebase patches
  * Update policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 01 Apr 2018 10:06:02 +0200

ros-rospack (2.4.3-1) unstable; urgency=medium

  * New upstream version 2.4.3
  * Update standards version

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 18 Oct 2017 08:14:41 +0200

ros-rospack (2.4.2-1) unstable; urgency=medium

  * New upstream version 2.4.2
  * Drop patch, applied upstream

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 12 Aug 2017 09:21:17 +0200

ros-rospack (2.4.1-2) unstable; urgency=medium

  * Simplify patches
  * Update dependency (Closes: #869181)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 02 Aug 2017 15:30:16 +0200

ros-rospack (2.4.1-1) unstable; urgency=medium

  * New upstream version 2.4.1
  * Update policy and debhelper versions
  * Rebase patches
  * Update watch file
  * Add patch for missing include

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 12 Jul 2017 18:54:39 +0200

ros-rospack (2.3.1-1) unstable; urgency=medium

  * Remove lintian override, fixed in lintian 2.5.47
  * New upstream version 2.3.1
  * Remove patch, accepted upstream

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 18 Sep 2016 16:47:24 +0200

ros-rospack (2.3.0-4) unstable; urgency=medium

  * Update URLs
  * Update my email address
  * Add Patch for FTBFS on hurd-i386.
    Thanks to Svante Signell (Closes: #835627)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 02 Sep 2016 10:58:10 +0200

ros-rospack (2.3.0-3) unstable; urgency=medium

  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Fix unit tests

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 26 Jul 2016 18:26:22 +0200

ros-rospack (2.3.0-2) unstable; urgency=medium

  * Add patch for hurd-i386.
    Thanks to Svante Signell (Closes: #822757)

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 18 Jun 2016 18:08:09 +0200

ros-rospack (2.3.0-1) unstable; urgency=medium

  * Imported Upstream version 2.3.0
  * Refresh patches

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 18 Jun 2016 16:57:00 +0200

ros-rospack (2.2.5-3) unstable; urgency=medium

  * Add patch to limit search depth

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 16 Feb 2016 11:09:26 +0100

ros-rospack (2.2.5-2) unstable; urgency=medium

  * Updated for multiarch-supporting catkin
  * Updated for renamed ros-cmake-modules

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Mon, 07 Dec 2015 13:41:48 +0000

ros-rospack (2.2.5-1) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Add patch for Debian specific SOVERSION and adopt package name
  * Install catkin and cmake files
  * Remove template from debian/copyright
  * Add lintian-overrides
  * Add dependency and description
  * Add patch for default ROS_PACKAGE_PATH
  * Imported Upstream version 2.2.5
  * Update build dependencies
  * Initial release (Closes: #804036)

  [ Leopold Palomo-Avellaneda ]
  * Update uploaders field

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Wed, 18 Nov 2015 19:19:09 +0000
